#ifndef WORKER_H
#define WORKER_H
#include <QObject>
#include <QFileInfo>
/**
 * OpenCV Image Alignment  Example
 *
 * Copyright 2015 by Satya Mallick <spmallick@learnopencv.com>
 *
 */

#include <QObject>
#include <QFileInfo>
#include <QDebug>
#include <QDesktopServices>
#include <QElapsedTimer>
#include "opencv2/opencv.hpp"


using namespace cv;
using namespace std;

class Worker : public QObject
{
    Q_OBJECT
    QElapsedTimer timer;
    QStringList extensions;
signals:
    void valueChanged(int newValue);
    void logMessage(QString message, int type);
    void finished();

public slots:
    void process();
    void stopProcessing();

public:
    Worker();
    ~Worker();
    QFileInfoList files;
    QString selectedMasterFilePath;
    QString selectedMethod;
    int selectedMethodIdx;
    Mat GetGradient(Mat src_gray);
    int runCoregistration();
};


#endif // WORKER_H
