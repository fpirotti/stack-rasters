#include <worker.h>
#include <opencv2/core/mat.hpp>
using namespace cv;
using namespace std;

Worker::Worker() {
    qDebug() << files;
    extensions << "jpg" << "JPG" << "TIF" << "tif";
    selectedMethodIdx = -1;
    selectedMethod = "";
    // you could copy data from constructor arguments to internal variables here.
}

void Worker::process() {
    qDebug() << files;
    qDebug() << this->selectedMasterFilePath;
    timer.restart();
    emit logMessage(QString("Processing started... Master image path is ").append(selectedMasterFilePath), 0);
    runCoregistration();
}

void Worker::stopProcessing() {

    emit logMessage( QString("Process stopped by user after ").append(QString::number(timer.elapsed()/1000)).append(" seconds."), 0);

    emit finished();
}



// --- DECONSTRUCTOR ---
Worker::~Worker() {
    qDebug() << "3333";
    // free resources
}

Mat Worker::GetGradient(Mat src_gray)
{
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    int scale = 1;
    int delta = 0;
    int ddepth = CV_32FC1; ;

    // Calculate the x and y gradients using Sobel operator

    Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );

    Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );

    // Combine the two gradients
    Mat grad;
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

    return grad;

}


int Worker::runCoregistration()
{
    if(selectedMethodIdx<0){
        emit logMessage( selectedMethod.append(QString(" method not set. ")) , 2);
        emit finished();
        return -1;
    }
    if( !extensions.contains(QFileInfo(selectedMasterFilePath).suffix()) ){
        emit logMessage( selectedMasterFilePath.append(QString(" does not seem to be an image file, "
"extension not recognized - only the following are recognized:<br>").append(extensions.join(", ")).append(
"<br>Skipping this file... ")), 2);
        emit finished();
        return -1;
    }
    Mat im_master =  imread(selectedMasterFilePath.toStdString() );
    QFileInfo fileinfo;
    int nfiles, nn=0;
    // Define the motion model
    const int warp_mode = MOTION_HOMOGRAPHY;
    Mat im_master_gray, im_slave_gray;
    cvtColor(im_master, im_master_gray, cv::COLOR_BGR2GRAY);
    // Specify the number of iterations.
    int number_of_iterations = 1000;

    // Specify the threshold of the increment
    // in the correlation coefficient between two iterations
    double termination_eps = 1e-10;

    // Define termination criteria
    TermCriteria criteria (TermCriteria::COUNT+TermCriteria::EPS, number_of_iterations, termination_eps);


    nfiles = files.count();
    for(nn=0; nn< nfiles; nn++){
        fileinfo = files.at(nn);
        emit  logMessage( fileinfo.fileName().append(QString(" processing.... ")), 0);
        emit valueChanged((nn+1)/(nfiles+1)*100);
        if(fileinfo.absoluteFilePath().contains(selectedMasterFilePath)){
            emit  logMessage( fileinfo.fileName().append(QString(" is the master file, skipping.... ")), 1);
            continue;
        }
        if( !extensions.contains(fileinfo.suffix()) ){
            emit logMessage( fileinfo.fileName().append(QString(" does not seem to be an image file! Skipping this file... ")), 1);
            //emit finished();
            continue;
        }

        QString outname = fileinfo.baseName();
        QString completeSuffix = QString(".").append(fileinfo.completeSuffix());
        qDebug() << outname;
        qDebug() << completeSuffix;
        qDebug() << fileinfo;

        /////////////////////////
        Mat im_slave =  imread(fileinfo.absoluteFilePath().toStdString().c_str());

        // Convert images to gray scale;
        cvtColor(im_slave, im_slave_gray, cv::COLOR_BGR2GRAY);

        // Set a 2x3 or 3x3 warp matrix depending on the motion model.
        Mat warp_matrix;

        // Initialize the matrix to identity
        if ( warp_mode == MOTION_HOMOGRAPHY )
            warp_matrix = Mat::eye(3, 3, CV_32F);
        else
            warp_matrix = Mat::eye(2, 3, CV_32F);


        // Run the ECC algorithm. The results are stored in warp_matrix.
        findTransformECC(
                         im_master_gray,
                         im_slave_gray,
                         warp_matrix,
                         warp_mode,
                         criteria
                     );

        // Storage for warped image.
        Mat im_slave_aligned;

        if (warp_mode != MOTION_HOMOGRAPHY)
            // Use warpAffine for Translation, Euclidean and Affine
            warpAffine(im_slave, im_slave_aligned, warp_matrix, im_master.size(), INTER_LINEAR + WARP_INVERSE_MAP);
        else
            // Use warpPerspective for Homography
            warpPerspective (im_slave, im_slave_aligned, warp_matrix, im_master.size(),INTER_LINEAR + WARP_INVERSE_MAP);

        QString oo = fileinfo.absolutePath().append(
                    outname.append("_Aligned_").append(selectedMethod).append(completeSuffix)
                    );
        qDebug() << oo;

        imwrite(oo.toStdString(), im_slave_aligned) ;

        /// ////////////////////
    }

    emit logMessage( QString("Process finished after ").append(QString::number(timer.elapsed()/1000)).append(" seconds."), 0);

    emit finished();
    // Show final output
    // waitKey(0);
    return(0);
}

