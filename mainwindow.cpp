#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    methods << "TRANSLATION" << "EUCLIDEAN" << "AFFINE" << "HOMOGRAPHY" << "SIFT WARPING";

    ui->comboBoxMethod->addItems(methods);
    ui->tableView->setModel(&model);

    QObject::connect( ui->pushButton , SIGNAL(released()),
                     this, SLOT(launchRegistration()));

    QObject::connect( ui->comboBoxMethod , SIGNAL(currentIndexChanged(int)),
                     this, SLOT(changeMethod(int)));


}


void MainWindow::launchRegistration()
{
//    if( model.rowCount() < 1 ){
//        QMessageBox::warning(this,QString("Warning"),QString("Files not present: drag and drop files in program and select master image."),
//                          QMessageBox::Ok);
//        logit("Files not present: drag and drop files  in windows.", 1);
//        return;
//    }
    //ui->progressBar->setValue(0);

    selection = ui->tableView->selectionModel()->selectedRows(3);

    if( selection.isEmpty() ){
        QMessageBox::warning(this,QString("Warning"),QString("Select master image."),
                          QMessageBox::Ok);
        logit("Select master image by clicking a row.", 1);
        return;
    }

    QModelIndex index = selection.at(0);

    QString  fi = index.data().value<QString>();

    QThread* thread = new QThread;
    workerThread = new Worker();
    workerThread->files =  files;
    workerThread->selectedMethodIdx =  ui->comboBoxMethod->currentIndex();
    workerThread->selectedMethod =  ui->comboBoxMethod->currentText();

    workerThread->selectedMasterFilePath = fi;
 //   this->ui->terminaButton->setEnabled(true);


    QObject::disconnect( ui->pushButton , SIGNAL(released()),
                     this, SLOT(launchRegistration()));

    ui->pushButton->setText("STOP");

    //connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

    QObject::connect( ui->pushButton , SIGNAL(released()),
                     workerThread, SLOT(stopProcessing()));

//    QObject::connect( workerThread, SIGNAL(finished()),
//                     this, SLOT(processingFinished()));

    workerThread->moveToThread(thread);

    connect(workerThread, SIGNAL(logMessage(QString, int)), this, SLOT(logit(QString, int)));
//    connect(workerThread, SIGNAL(resultReady(QStringList)), this, SLOT(setAddresses(QStringList)));
    connect(workerThread, SIGNAL(valueChanged(int)),  this, SLOT(setProgress(int)));
    connect(workerThread, SIGNAL(finished()), this, SLOT(processingFinished()));
//    connect(workerThread, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(started()), workerThread, SLOT(process()));
    thread->start();
}


void MainWindow::changeMethod(int index)
{
    logit(QString("Method set to ").append(ui->comboBoxMethod->currentText() ), 1);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent *e)
{

    files.clear();
    // ui->tableView.mo;
    int i=1;
    foreach (const QUrl &url, e->mimeData()->urls()) {
        model.append(QFileInfo(url.toLocalFile()));
        files.append(QFileInfo(url.toLocalFile()));
        //model.setHeaderData(i, Qt::Vertical, i);
        i++;
    }
    ui->tableView->setModel(&model);
}

void MainWindow::setProgress(int val)
{
    ui->progressBar->setValue( val );
}

void MainWindow::processingFinished()
{

    QObject::disconnect( ui->pushButton , SIGNAL(released()),
                     workerThread, SLOT(stopProcessing()));

    QObject::disconnect( workerThread, SIGNAL(finished()),
                     this, SLOT(processingFinished()));


    ui->pushButton->setText("Co-Register images");

    QObject::connect( ui->pushButton , SIGNAL(released()),
                     this, SLOT(launchRegistration()));

    ui->progressBar->setRange(0, 100);
    ui->progressBar->setValue(0);
    ui->textBrowser->append("Processo terminato");
    ui->textBrowser->append("================");
}

void MainWindow::logit(QString mes, int type)
{
    if(type==2)  mes.prepend("<font color=red>").append("</font>");
    if(type==1)  mes.prepend("<font color=orange>").append("</font>");
    ui->textBrowser->append(mes);
}

MainWindow::~MainWindow()
{
    delete ui;
}



